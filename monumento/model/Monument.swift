//
//  ARObject.swift
//  monumento
//
//  Created by Yasith Jayawardana on 3/26/20.
//  Copyright © 2020 AOSSIE. All rights reserved.
//

import Foundation
import RealityKit

class Monument {
    
    public var name: String
    public var model: ModelEntity
    public var description: String
    public var location : SIMD3<Double>
    
    init(model: ModelEntity, name: String, description: String, location: SIMD3<Double>) {
        self.name = name
        self.model = model
        self.description = description
        self.location = location
        self.model.name = name
    }
}
