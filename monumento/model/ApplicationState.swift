//
//  DataModel.swift
//  monumento
//
//  Created by Yasith Jayawardana on 3/25/20.
//  Copyright © 2020 AOSSIE. All rights reserved.
//

import Combine
import RealityKit
import CoreLocation

class ApplicationState: ObservableObject {
    
    @Published var x: Double = 0 {didSet {updateARLocation()}}
    @Published var y: Double = 0 {didSet {updateARLocation()}}
    @Published var z: Double = 0 {didSet {updateARLocation()}}
    @Published var arView: ARView
    
    private var monuments: [Monument] = []
    private var anchor = AnchorEntity(world: SIMD3<Float>(0,0,0))
    private var location: SIMD3<Double>?
    private var heading: SIMD3<Double>?
    
    init() {
        // Initialize ARView
        self.arView = ARView(frame: .zero)
        // Add anchor
        self.arView.scene.addAnchor(anchor)
        // Create temporary monuments (for now)
        self.monuments.append(createMonument(name: "monument 1", description: "description 1"))
        // Add monuments to anchor
        self.monuments.forEach({ monument in anchor.addChild(monument.model)})
    }
    
    func updateARLocation() {
        if let location = self.location {
            if let heading = self.heading {
                self.monuments.forEach({monument in
                    // Move monument to current location
                    monument.location = location
                    // Get relative location for monument
                    let rLocation = monument.location - location
                    // Get relative heading
                    let arLoc = SIMD3<Double>(x,y,z)
                    let rHeading = heading - arLoc
                    let translation = SIMD3<Float>(arLoc)
                    if let entity = arView.scene.findEntity(named: monument.model.name) {
                        entity.transform.translation = translation
                    }
                })
            }
        }
    }
    
    func updateCurrentLocation(location: SIMD3<Double>) {
        self.location = location
        print("Location: \(location)")
    }
    
    func updateCurrentHeading(heading: SIMD3<Double>) {
        self.heading = heading
        print("Heading: \(heading)")
    }
    
    func getRelativeLocation(objectLocation: SIMD3<Double>, currentLocation: SIMD3<Double>) -> SIMD3<Double>{
        return objectLocation - currentLocation
    }
    
}
