//
//  DataModel.swift
//  monumento
//
//  Created by Yasith Jayawardana on 3/25/20.
//  Copyright © 2020 AOSSIE. All rights reserved.
//

import Combine
import RealityKit

class ApplicationState: ObservableObject {
    
    static var shared = ApplicationState()
    
    @Published var xTranslation: Float = 0 {didSet {translateBox()}}
    @Published var yTranslation: Float = 0 {didSet {translateBox()}}
    @Published var zTranslation: Float = 0 {didSet {translateBox()}}
    
    var arObject: AnchorEntity
    
    init() {
        let box = MeshResource.generateBox(size: 0.3)
        let material = SimpleMaterial(color: .blue, isMetallic: true)
        let entity = ModelEntity(mesh: box, materials: [material])
        entity.name = "Box"
        
        arObject = AnchorEntity()
        arObject.addChild(entity)
        
        arView.scene.addAnchor(arObject)
    }
    
    func translateBox() {
        let box = arView.scene.findEntity(named: "Box")
        let translation = SIMD3<Float>(xTranslation, yTranslation, zTranslation)
        box?.transform.translation = translation
    }
    
}
