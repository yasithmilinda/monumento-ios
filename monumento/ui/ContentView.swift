//
//  ContentView.swift
//  monumento
//
//  Created by Yasith Jayawardana on 3/25/20.
//  Copyright © 2020 AOSSIE. All rights reserved.
//

import SwiftUI
import RealityKit

struct ContentView : View {
    
    @EnvironmentObject var state: ApplicationState
    
    var body: some View {
        NavigationView {
            MenuUI()
        }
    }
}

struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(ApplicationState())
    }
}
