//
//  M3DView.swift
//  monumento
//
//  Created by Yasith Jayawardana on 3/27/20.
//  Copyright © 2020 AOSSIE. All rights reserved.
//

import SwiftUI
import SceneKit
import RealityKit

struct MInteractiveView: UIViewRepresentable {
    
    private var model: ModelEntity
    private var scene: SCNScene
    
    init(model: ModelEntity) {
        self.model = model
        self.scene = SCNScene()
    }
    
    func makeUIView(context: Context) -> SCNView {
        let sceneView = SCNView()
        
        // add box
        let boxGeometry = SCNBox(width: 10.0, height: 10.0, length: 10.0, chamferRadius: 1.0)
        let boxNode = SCNNode(geometry: boxGeometry)
        self.scene.rootNode.addChildNode(boxNode)
        
        // add camera
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        self.scene.rootNode.addChildNode(cameraNode)
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 30)
        
        // add light
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = .omni
        lightNode.position = SCNVector3(x: 0, y: 10, z: 10)
        self.scene.rootNode.addChildNode(lightNode)
        
        // add ambient light
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = .ambient
        ambientLightNode.light!.color = UIColor.darkGray
        self.scene.rootNode.addChildNode(ambientLightNode)
        
        // set scene to SCNView
        sceneView.scene = self.scene
        return sceneView
    }
    
    func updateUIView(_ scnView: SCNView, context: Context) {
        // allows the user to manipulate the camera
        scnView.allowsCameraControl = true
        // configure the view
//        scnView.backgroundColor = UIColor.black
    }
}

struct M3DView: View {
    
    private var model: ModelEntity
    
    init(model: ModelEntity) {
        self.model = model
    }
    
    var body: some View {
        MInteractiveView(model: model)
    }
}

struct M3DView_Previews: PreviewProvider {
    static var previews: some View {
        let model = createModel()
        return M3DView(model: model)
    }
}
