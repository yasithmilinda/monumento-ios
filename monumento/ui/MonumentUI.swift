//
//  MonumentUI.swift
//  monumento
//
//  Created by Yasith Jayawardana on 3/27/20.
//  Copyright © 2020 AOSSIE. All rights reserved.
//

import SwiftUI

struct MonumentUI: View {
    
    private var monument: Monument
    
    init(monument: Monument) {
        self.monument = monument
    }
    
    var body: some View {
        VStack {
            HStack {
                VStack(alignment: .leading) {
                    Text(monument.name).font(.title)
                    Text(monument.description)
                }
                Spacer()
            }
            M3DView(model: self.monument.model)
            Spacer()
        }.padding()
    }
}

struct MonumentUI_Previews: PreviewProvider {
    static var previews: some View {
        let monument = createMonument(name: "Statue of Liberty", description: "Lorem Ipsum")
        return MonumentUI(monument: monument)
    }
}
