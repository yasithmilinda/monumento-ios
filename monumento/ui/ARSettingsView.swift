//
//  ARUIView.swift
//  monumento
//
//  Created by Yasith Jayawardana on 3/25/20.
//  Copyright © 2020 AOSSIE. All rights reserved.
//

import SwiftUI

struct ARSettingsView: View {
    
    @EnvironmentObject var state: ApplicationState
    
    var body: some View {
        VStack {
            Text("Adjust X,Y,Z to move the object around once it appears")
                Slider(value: $state.x, in: -2...2, step: 0.01)
            Text("X: \(state.x)")
                Slider(value: $state.y, in: -2...2, step: 0.01)
            Text("Y: \(state.y)")
                Slider(value: $state.z, in: -2...2, step: 0.01)
            Text("Z: \(state.z)")
        }
        .padding()
    }
}

struct ARSettingsView_Previews: PreviewProvider {
    static var previews: some View {
        ARSettingsView().environmentObject(ApplicationState())
    }
}
