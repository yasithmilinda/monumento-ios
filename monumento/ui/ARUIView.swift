//
//  ARViewContainer.swift
//  monumento
//
//  Created by Yasith Jayawardana on 3/25/20.
//  Copyright © 2020 AOSSIE. All rights reserved.
//

import SwiftUI
import RealityKit

struct ARUIView: UIViewRepresentable {
    
    @EnvironmentObject var state: ApplicationState
    
    func makeUIView(context: Context) -> ARView {
        return state.arView
    }
    
    func updateUIView(_ uiView: ARView, context: Context) {}
}

struct ARUIView_Previews: PreviewProvider {
    static var previews: some View {
        Text("UI unavailable in Preview Mode")
    }
}
