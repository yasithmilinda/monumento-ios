//
//  MenuUI.swift
//  monumento
//
//  Created by Yasith Jayawardana on 3/27/20.
//  Copyright © 2020 AOSSIE. All rights reserved.
//

import SwiftUI

struct MenuUI: View {
    
    @State var active = true;
    
    var body: some View {
        Form {
            Section {
                NavigationLink(destination: CameraUI(), isActive: $active) {
                    HStack {
                        Text("Camera")
                        Spacer()
                        Text("")
                    }.padding(.vertical)
                }
                HStack {
                    Text("Travel Log")
                    Spacer()
                    Text("")
                }.padding(.vertical)
                HStack {
                    Text("Monument Map")
                    Spacer()
                    Text("")
                }.padding(.vertical)
            }
            Section(footer: HStack {
                Spacer()
                VStack {
                    Text("Monumento v1.0.0")
                    Text("AOSSIE")
                }.padding()
                Spacer()
            }){
                HStack {
                    Text("Settings")
                    Spacer()
                    Text("")
                }.padding(.vertical)
                HStack {
                    Text("Report an Issue")
                    Spacer()
                    Text("")
                }.padding(.vertical)
                HStack {
                    Text("About")
                    Spacer()
                    Text("")
                }.padding(.vertical)
            }
        }.navigationBarTitle("Monumento")
    }
}

struct MenuUI_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            MenuUI()
        }
    }
}
