//
//  CameraUI.swift
//  monumento
//
//  Created by Yasith Jayawardana on 3/26/20.
//  Copyright © 2020 AOSSIE. All rights reserved.
//

import SwiftUI

struct CameraUI: View {
    
    @State var monumentSelected: Bool = false
    
    // Set hardcoded monument for now
    private var selectedMonument: Monument = createMonument(
        name: "Statue of Liberty",
        description: "Some Description"
    )
    
    var body: some View {
        VStack {
            ARUIView().onTapGesture {
                self.monumentSelected = true
            }.edgesIgnoringSafeArea(.all)
            ARSettingsView()
        }
        .navigationBarTitle(Text("Camera"), displayMode: .inline)
        .sheet(isPresented: $monumentSelected, onDismiss: {
            self.monumentSelected = false
        }){
            MonumentUI(monument: self.selectedMonument)
        }
    }
}

struct CameraUI_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            CameraUI().environmentObject(ApplicationState())
        }
    }
}
