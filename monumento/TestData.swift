//
//  TestBed.swift
//  monumento
//
//  Created by Yasith Jayawardana on 3/27/20.
//  Copyright © 2020 AOSSIE. All rights reserved.
//

import Foundation
import RealityKit

func createMonument(name: String, description: String) -> Monument {
    let model = createModel()
    let location = SIMD3<Double>(0,0,0)
    return Monument(model: model, name: name, description: description, location: location)
}

func createModel() -> ModelEntity {
    let box = MeshResource.generateBox(size: 0.3)
    let material = SimpleMaterial(color: .blue, isMetallic: true)
    let model = ModelEntity(mesh: box, materials: [material])
    return model
}
