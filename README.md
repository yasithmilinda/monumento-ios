# Monumento (iOS)

This project was created to demonstrate the proof of concept that I proposed in AOSSIE/monumento for GSoC 2020.
It uses SwiftUI, ARKit, CoreLocation, and RealityKit to render 3D models on a certain GPS Coordinate (Longitude, Latitude, Altitude).
The goal is for the app to dynamically place an AR object given its GPS coordinates (if the user is close to that),
and to give the user the option to click on it and view the 3D model in depth for educational purposes.

I plan to build as much screens as I can for this application.
However, my implementation timeline spans across GSoC, so it would be far from complete at this stage.

## Current Implementation
![Imgur](https://i.imgur.com/nLbFSrF.gif)